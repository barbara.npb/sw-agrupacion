Desarrollo de Sistema para el control de Asociaciones estudiantiles de la Universidad del Bío-Bío  

En este sistema los alumnos pueden postular asociaciones estudiantiles, solicitar fondos para las asociaciones y hacer las rendiciones de gastos.

Factibilidad técnica: 

-El computador debe contar con los siguientes programas.
-Apache (en caso de ser usado en linux)
-Php 7.2 o superior 
-Composer 
-Wamp (en caso de ser usado en windows)

//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//

Instrucciones para la instalación de los software en Linux

Apache :  sudo apt -get install apache2

php 7.2: sudo apt-get install php7.2 php7.2-zip php7.2-mbstring phpunit

Composer

Documentacion  https://getcomposer.org/download/

Ejecutar en la consola

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

php -r "if (hash_file('sha384', 'composer-setup.php')==='93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt';unlink('composersetup.php'); } echo PHP_EOL;"

php composer-setup.php

php -r "unlink('composer-setup.php');"

Para configurar identidad de git, escribir lo siguiente en la consola: 

 git config --global user.name “Nombre"
 git config --global user.email email

git config --list (Para comprobar identidad)

 git clone http://gitlab.face.ubiobio.cl:8081/17862709/sw-asociacion.git * 

cd sw-asociacion  

composer update 

composer install (en caso de inconvenientes) 

php artisan serve 

Abrir el navegador en  la siguiente pagina: 
(el puerto puede variar, según los disponibles, en ese caso cambiar por el que muestra la terminal) 

http://localhost:8000/index


//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//

Instrucciones para la instalación de software en Windows

Links de descarga de los software: 

wamp  
64 bits: https://sourceforge.net/projects/wampserver/files/WampServer%203/WampServer%203.0.0/wampserver3.1.9_x64.exe/download 
32 bits: https://sourceforge.net/projects/wampserver/files/WampServer%203/WampServer%203.0.0/wampserver3.1.9_x86.exe/download
 
git 
https://github.com/git-for-windows/git/releases/download/v2.22.0.windows.1/Git-2.22.0-64-bit.exe

composer 
https://getcomposer.org/download/1.8.6/composer.phar



Configurar php en wamp haciendo click en el icono de wamp (iconos de inicio) seleccionando php 7.2.14 o superior 


Ingresar a C:\wamp64\www   hacer click derecho, abrir la consola “Git bach here” . Escribir los siguientes comandos 

Para configurar identidad de git: 

 git config --global user.name “Nombre"
 git config --global user.email email

git config --list (Para comprobar identidad)

 git clone http://gitlab.face.ubiobio.cl:8081/17862709/sw-asociacion.git 

escribir las credenciales de gitubb  (en caso de ser solicitadas)

Una vez terminado el proceso, entrar a la nueva carpeta generada, abriendo nuevamente la consola “Git bach here”. Escribir los siguientes comandos:

composer update 
composer install (en caso de presentar problemas en el update) 
php artisan serve

Abrir navegador y ingresar a localhost:8000/index


//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//



Observaciones: No es necesario instalar laravel, ya que no se comenzará con un proyecto nuevo.
                      

