<?php
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('index',function(){
 	return view('index');
});

Route::get('/', function () {
    return view('auth.login');
});


Route::get('app', function(){
        return view('layouts.app');
    });
Route::get('appa', function(){
        return view('layouts.appAuth');
    });

Route::get('verify', function(){
	return view('auth.verify');
});


//auth 
Route::prefix('auth')->group(function(){
	//Route::post('createRegister','Auth\RegisterController@create')->name('createRegister');
	Route::get('logout','Auth\LoginController@logout')->name('logout');
	Route::get('verRegistro','Auth\RegisterController@verRegistro')->name('verRegistro');

});


//Agrupaciones

Route::prefix('association')->group(function(){

    Route::get('postulationAssociation','View\AssociationViewController@viewPostulation')->name('postulationAssociation');
	Route::post('create','View\AssociationViewController@create')->name('create');
	Route::get('report','View\AssociationViewController@report')->name('report');
	Route::get('directivaA','View\AssociationViewController@directivaA')->name('directicaA');
	Route::get('showReport/{agrupacion}','View\AssociationViewController@showReport')->name('showReport');
	Route::get('memberAssociation/{agrupacion}','View\AssociationViewController@memberAssociation')->name('memberAssociation');
	Route::get('verAssciation','View\AssociationViewController@viewPostulation')->name('postulationAssociation');
	Route::get('consultAssociations', 'View\AssociationViewController@consultAssociations')->name('consultAssociations');
	Route::get('viewConsult', 'View\AssociationViewController@viewConsult')->name('viewConsult');
	Route::get('associationStatus', 'View\AssociationViewController@readAssociation')->name('associationStatus');
	Route::get('delete/{agrupacion}','View\AssociationViewController@deleteAssociation')->name('delete');
	Route::post('updateAs', 'View\AssociationViewController@updateAs')->name('updateAs');
	Route::get('formEdit/{agrupacion}','View\AssociationViewController@formEdit')->name('formEdit');
	Route::get('listAssociation', 'View\AssociationViewController@listAssociation')->name('listAssociation');


});

//Solicitud de dinero
Route::prefix('requestmoney')->group(function(){
Route::get('requestmoney', 'View\MoneyrequestViewController@requestMoney')->name('viewRequest');
Route::get('history', 'View\MoneyrequestViewController@requestHistory')->name('viewHistory');
Route::get('status', 'View\MoneyrequestViewController@requestStatus')->name('viewStatus');
Route::post('addSolicitud', 'View\MoneyrequestViewController@addSolicitud')->name ('addSolicitud');
Route::get('readRequest', 'View\MoneyrequestViewController@readRequest')->name('readRequest');
Route::get('destroy/{idSolicitud}', 'View\MoneyrequestViewController@destroy')->name('destroy');
Route::get('update/{idSolicitud}', 'View\MoneyrequestViewController@update')->name('update');
Route::post('history', 'View\MoneyrequestViewController@updateP')->name('updateP');
Route::get('readEstados', 'View\MoneyrequestViewController@readEstados')->name('readEstados');
});



//mantenedor
Route::prefix('maintainer')->group(function(){
	Route::get('carrera', 'View\MaintainerViewController@viewCarrera')->name('carrera');
	Route::get('alumno', 'View\MaintainerViewController@viewAlumno')->name('alumno');
	Route::get('admin', 'View\MaintainerViewController@viewAdmin')->name('admin');
	Route::get('estado', 'View\MaintainerViewController@viewEstado')->name('estado');
	Route::post('storeStudent', 'View\MaintainerViewController@storeStudent')->name('storeStudent');
	Route::get('readStudent', 'View\MaintainerViewController@readStudent')->name('readStudent');
	Route::get('statusTypeData', 'View\MaintainerViewController@statusTypeData')->name('statusTypeData');
	Route::get('readAdmin', 'View\MaintainerViewController@readAdmin')->name('readAdmin');
	Route::get('readCarrera', 'View\MaintainerViewController@readCarrera')->name('readCarrera');
	Route::get('readEstado', 'View\MaintainerViewController@readEstado')->name('readEstado');
});

 //actividades 

Route::prefix('activities')->group(function(){
	Route::get('activities','View\ActivitiesViewController@viewActivities')->name('activities');
	Route::get('history', 'View\ActivitiesViewController@activitiesHistory')->name('historyActiv');
	Route::get('readActivities', 'View\ActivitiesViewController@readActivities')->name('readActivities');
	Route::post('addActivities','View\ActivitiesViewController@addActivities')->name('addActivities');
});
//Rendicion de cuentas

Route::prefix('accountability')->group(function(){
	Route::get('accountability','View\AccountabilityViewController@accountability')->name('accountability');
	Route::get('history','View\AccountabilityViewController@accountabilityHistory')->name('histotyAcc');
	Route::get('status','View\AccountabilityViewController@accountabilityStatus')->name('statusAcc');
	Route::get('readAccountability','View\AccountabilityViewController@readAccountability')->name('readAccountability');

	Route::post('addAccountability','View\AccountabilityViewController@addAccountability')->name('addAccountability');
	Route::get('readStatus','View\AccountabilityViewController@readStatus')->name('readStatus');
	Route::put('editAccountability','View\AccountabilityViewController@editAccountability')->name('editAccountability');
	Route::get('deleteAccountability/{accountability}','View\AccountabilityViewController@deleteAccountability')->name('deleteAccountability');

});