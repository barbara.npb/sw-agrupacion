<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table = 'Cargo';
    protected $primaryKey = 'idCargo';
    public $timestamps = false; 
    protected $fillable = array(

        'idCargo'
        'idAlumno', 
        'idAgrupacion' 
    );
    public function directiva(){

        return $this->belongsTo('App\Directiva');
    }
}
