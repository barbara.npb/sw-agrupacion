<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facultad extends Model
{
    protected $table = 'Facultad';
    protected $primaryKey = 'idfacultad';
    public $timestamps = false; 
    protected $fillable = array(

        'idfacultad',
        'NombreFacultad',
    );
    public function carreras(){

        return $this->hasMany('App\Carrera');
    }
     public function ubicadas(){

        return $this->hasMany('App\Ubicada');
    }

}
