<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ubicada extends Model
{
    protected $table = 'Ubicada';
    protected $primaryKey = 'idUbicada';
     public $timestamps = false; 
    protected $fillable = array(

        'idUbicada',
        'idFacultad',
        'idSede',

    );
     public function facultad(){

        return $this->belongsTo('App\Facultad');
    }
     public function sede(){

        return $this->belongsTo('App\Sede');
    }
}
