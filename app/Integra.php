<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Integra extends Model
{
    protected $table = 'Integra';
    protected $primaryKey = 'idIntegra';

    public $timestamps = false; 

    protected $fillable = array(

        'idIntegra',
        'idAlumno',
        'idAgrupacion',

    );
    public function alumno(){

        return $this->belongsTo('App\Alumno');
    }
    public function agrupacion(){

        return $this->belongsTo('App\Agrupacion');
    }
}

