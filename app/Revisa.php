<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revisa extends Model
{
    protected $table = 'Revisa';
    protected $primaryKey = 'idRevisa';
    public $timestamps = false; 
    protected $fillable = array(

        'idRevisa',
        'idRendicion',
        'idAdmin',
        'Estado',

    );
    public function rendicion(){

        return $this->belongsTo('App\RendicionCuenta');
    }
    public function administrador(){

        return $this->belongsTo('App\Administrador');
    }
}
