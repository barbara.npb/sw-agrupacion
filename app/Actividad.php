<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $table = 'Actividades';
    protected $primaryKey = 'idActividad';
    public $timestamps = false; 
    protected $fillable = array(

        'idActividad',
        'idDirectiva',
        'Nombre',
        'Fecha',
        'ObjetivoActividad',
        
    );
    public function directiva(){

        return $this->belongsTo('App\Directiva');
    }
}
