<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NivelSancion extends Model
{
    protected $table = 'NivelSancion';
    protected $primaryKey = 'idNivel';
    public $timestamps = false; 
    protected $fillable = array(

    	'idNivel'
        'nombreSancion',
    );
     public function sanciones(){

        return $this->hasMany('App\Sancion');
    }
}
