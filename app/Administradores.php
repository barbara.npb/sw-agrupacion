<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Administradores extends Model
{
    protected $table = 'Administradores';
    public $timestamps = false; 
    protected $primaryKey = 'idAdmin';
    protected $fillable = array(

        'idAdmin',
        'Nombre',
        'Cargo',
        'idTipoAdmin',
    );
    public function tipoAdmin(){

        return $this->belongsTo('App\TipoAdmin');
    }
    public function evaluaAceptaciones(){

        return $this->hasMany('App\EvaluaAceptacion');
    }
     public function revisas(){

        return $this->hasMany('App\Revisa');
    }
     public function verificas(){

        return $this->hasMany('App\Verifica');
    }
}
