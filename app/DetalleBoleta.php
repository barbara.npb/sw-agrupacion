<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleBoleta extends Model
{
    protected $table = 'DetalleBoleta';
    protected $primaryKey = 'idDetalle';
    public $timestamps = false; 
    protected $fillable = array(

        'idDetalle',
        'NumeroBoleta',
        'GiroComercial',
        'Producto',
        'Cantidad',
        'ValorProducto',
        'MontoTotal',
        'ConceptoCompra',
        'idRendicion',

    );
    public function rendicion(){

        return $this->belongsTo('App\RendicionCuenta');
    }
}
