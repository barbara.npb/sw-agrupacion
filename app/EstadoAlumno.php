<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoAlumno extends Model
{
    protected $table = 'estadoAlumno';
    protected $primaryKey = 'idEstado';
    public $timestamps = false; 
    protected $fillable = array(

        'idEstado',
        'NombreEstado',
    );
    public function alumnos(){

        return $this->hasMany('App\Alumno');
    }
}
