<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agrupacion extends Model
{
    protected $table = 'Agrupacion';
    protected $primaryKey = 'idAgrupacion';
    public $timestamps = false; 
    protected $fillable = array(

        'idAgrupacion',
        'NombreAgrupacion',
        'Descripcion',
        'ObjetvoAgrupacion',
        'TiempoDuracion',
        'idEstadoA',
        'idDirectiva',

    );
    public function estadoagrupacion(){

        return $this->belongsTo('App\EstadoAgrupacion');
    }
    public function directiva(){

        return $this->belongsTo('App\Directiva');
    }
    public function evaluaAceptaciones(){

        return $this->hasMany('App\EvaluaAceptacion');
    }
    public function integras(){

        return $this->hasMany('App\Integra');
    }
    public function cargo(){

        return $this->hasMany('App\Cargo');
    }

}
