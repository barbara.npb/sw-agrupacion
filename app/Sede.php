<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sede extends Model
{
    protected $table = 'Sede';
    protected $primaryKey = 'idSede';
    public $timestamps = false; 
    protected $fillable = array(

        'idSede',
        'NombreSede',
        'Ciudad',
    );
     public function ubicadas(){

        return $this->hasMany('App\Ubicada');
    }
}
