<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudia extends Model
{
    protected $table = 'Estudia';
    protected $primaryKey = 'idEstudia';
    public $timestamps = false; 
    protected $fillable = array(

        'idEstudia',
        'idAlumno',
        'idCarrera',

    );
}
