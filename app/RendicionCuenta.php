<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RendicionCuenta extends Model
{
    use SoftDeletes;

    protected $table = 'RendicionCuentas';
    protected $primaryKey = 'idRendicion';
    public $timestamps = false; 
    protected $fillable = array(

        'idRendicion',
        'MontoEntregado',
        'FechaDeposito',
        'ItemGastado',
        'idDirectiva',
        'file',

    );
    protected $dates =['delete_at'];

     public function directiva(){

        return $this->belongsTo('App\Directiva');
    }
    public function detalleBoleta(){

        return $this->hasMany('App\DetalleBoleta');
    }
     public function revisa(){

        return $this->hasMany('App\Revisa');
    }
   
}
