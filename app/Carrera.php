<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{
    protected $table = 'Carrera';
    protected $primaryKey = 'idCarrera';
    public $timestamps = false; 
    protected $fillable = array(

        'idCarrera',
        'CodigoCarrera',
        'idFacultad',
        'NombreCarrera',
        'SemestresDuracion',
        
        
    );
    public function facultad(){

        return $this->belongsTo('App\Facultad');
    }
}
