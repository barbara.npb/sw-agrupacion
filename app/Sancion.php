<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sancion extends Model
{
    protected $table = 'Sancion';
    protected $primaryKey = 'idSancion';
    protected $fillable = array(

        'idSancion',
        'Descripcion',
        'idNivel',
        'idAlumno',

    );
     public function nivelSancion(){

        return $this->belongsTo('App\NivelSancion');
    }
     public function alumno(){

        return $this->belongsTo('App\Alumno');
    }
}
