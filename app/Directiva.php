<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Directiva extends Model
{
    protected $table = 'Directiva';
    protected $primaryKey = 'idDirectiva';
    public $timestamps = false; 
    protected $fillable = array(

        'idDirectiva',
        'NombreRepresentacion',
        'idRol',
    );
     public function rol(){

        return $this->belongsTo('App\RolDirectiva');
    }
    public function actividades(){

        return $this->hasMany('App\Actividad');
    }
    public function agrupaciones(){

        return $this->hasMany('App\Agrupacion');
    }
    public function rendicionCuentas(){

        return $this->hasMany('App\RendicionCuenta');
    }
     public function solicitudDineros(){

        return $this->hasMany('App\SolicitudDinero');
    }
}
