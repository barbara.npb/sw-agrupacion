<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolDirectiva extends Model
{
    protected $table = 'rolDirectiva';
    protected $primaryKey = 'idRol';
    public $timestamps = false; 
    protected $fillable = array(

        'idRol',
        'NombreRol',
    );
     public function directivas(){

        return $this->hasMany('App\Directiva');
    }
}
