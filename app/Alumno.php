<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model{

    protected $table = 'Alumno';
    protected $primaryKey = 'idAlumno';
    public $timestamps = false; 
    protected $fillable = array(
    

        'idAlumno',
        'rutAlumno',
        'Nombre',
        'ApellidoPaterno',
        'ApellidoMaterno',
        'Email',
        'Celular',
        'DireccionAcademica',
        'AñoIngreso',
        'idEstado'


    );
    public function estadoAlumno(){

        return $this->belongsTo('App\EstadoAlumno');
    }
    public function integra(){

        return $this->hasMany('App\Integra');
    }
     public function sanciones(){

        return $this->hasMany('App\Sancion');
    }
      public function cargo(){

        return $this->hasMany('App\Cargo');
    }
}
