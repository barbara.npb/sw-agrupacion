<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class DuracionAgrupacion extends Model
{

    protected $table = 'DuracionAgrupacion';
    protected $primaryKey = 'idTiempo';
    public $timestamps = false; 
    protected $fillable = array(

        'idTiempo',
        'TiempoDuracion'

    );
 
    
   
    public function agrupacion(){

        return $this->hasMany('App\Agrupacion');
    }
   

}
