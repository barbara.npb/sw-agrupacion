<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoAgrupacion extends Model
{
    protected $table = 'EstadoAgrupacion';
    protected $primaryKey = 'idEstadoA';
    public $timestamps = false; 
    protected $fillable = array(

    	'idEstadoA',
        'NombreEstadoA',
    );
}
}
