<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verifica extends Model
{
    protected $table = 'Verifica';
    protected $primaryKey = 'idVerifica';
     public $timestamps = false; 
    protected $fillable = array(

        'idVerifica',
        'idAdmin',
        'idSolicitud',
        'Estado'

    );
     public function administrador(){

        return $this->belongsTo('App\Administrador');
    }
     public function solicitud(){

        return $this->belongsTo('App\SolicitudDinero');
    }
}
