<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAdmin extends Model
{
    protected $table = 'TipoAdmin';
    protected $primaryKey = 'idTipoAdmin';
    public $timestamps = false; 
    protected $fillable = array(

        'idTipoAdmin',
        'NombreTipoAdmin',
    );
    public function administrador(){

        return $this->hasMany('App\Administrador');
    }
}
