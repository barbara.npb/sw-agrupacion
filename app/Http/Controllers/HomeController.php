<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class HomeController extends Controller
{
    
     
    public function app(){

        return view('layouts.app');
    }

    public function __construct(){

        $this->middleware('auth');
    }

    public function index(Request $request){
        
        $request->user()->authorizeRoles(['user', 'admin']);
        return view('home');
    }

    public function doLogout(){

    Auth::logout(); // sale de la aplicación
    return Redirect::to('login'); // redirige pestaña principal
    }
 

}
