<?php

namespace App\Http\Controllers\View;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Alumno;
use App\Agrupacion;
use App\Actividad;
use App\Directiva;

class ActivitiesViewController extends Controller
{
    //
    public function viewActivities(){
      $directiva = Directiva::all();
        return view('activities.activities', compact('directiva'));
    }

    public function activitiesHistory(){
        return view('activities.activitiesHistory');
    }

    
    public function addActivities(Request $request){
      //dd($request);
     // $idActividad= uniqid();
    	$actividad = new Actividad;
  		$actividad->idDirectiva=$request->all()['idDirectiva'];
  		$actividad->Nombre=$request->all()['Nombre'];
  		$actividad->Fecha=$request->all()['Fecha'];
  		$actividad->ObjetivoActividad=$request->all()['ObjetivoActividad'];
     
  		        
        $actividad->save();
  
        return view('activities.activitiesHistory');
    }

    public function readActivities(){
        $actividad = Actividad::all();
        $data['data'] = $actividad;
        return $data;
    }
}




