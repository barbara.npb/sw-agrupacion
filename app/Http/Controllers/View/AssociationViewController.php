<?php

namespace App\Http\Controllers\View;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Query\Builder;
use App\Alumno;
use App\Integra;
use App\Agrupacion;
use App\Directiva;
use App\Actividades;
use App\Cargo;
use App\Estudia;
use App\rolDirectiva;
use App\DuracionAgrupacion;
use App\EstadoAgrupacion;
use Carbon\Carbon;


class AssociationViewController extends Controller
{
    public function viewPostulation(){

            $agrupacions = DuracionAgrupacion::all();

        return view('association.postulationAssociation', compact('agrupacions'));
    }

        
    public function create (Request $request){

          $rules = [
        'NombreAgrupacion' => 'required|min:2|max:49|unique:Agrupacion',
        'Descripcion' => 'required|min:20|max:150',
        'ObjetivoAgrupacion' => 'required|min:20|max:1000',
        
    ];

    
         $messages = [
    'NombreAgrupacion.required' => 'Debes agregar un nombre a la agrupacion',
    'NombreAgrupacion.min' =>'El nombre de la agrupacion debe ser mayor a :min caracteres.',
    'NombreAgrupacion.max' =>'El nombre de la agrupacion debe ser nemor a :max caracteres.',
    'NombreAgrupacion.unique' => 'Ya existe una agrupacion con ese nombre',
    'Descripcion.required' => 'Debes agregar la descripcion de la agrupacion.',
    'Descripcion.min' => 'La descripcion de la agrupacion debe ser mayor a :min caracteres.',
    'Descripcion.max' => 'La descripcion de la agrupacion debe ser menor a :max caracteres.',
    'ObjetivoAgrupacion.required' => 'Debes agregar el objetivo de la agrupacion',
    'ObjetivoAgrupacion.min' =>'El objetivo de la agrupacion debe ser mayor a :min caracteres.',
    'ObjetivoAgrupacion.max' =>'El objetivo de la agrupacion debe ser menor a :max caracteres.',
    
];
 
    $this->validate($request, $rules, $messages);

            $Agrupacion = new Agrupacion();
           


            $Agrupacion->NombreAgrupacion = $request->NombreAgrupacion;
            $Agrupacion ->Descripcion = $request->Descripcion;
            $Agrupacion ->ObjetivoAgrupacion = $request->ObjetivoAgrupacion;
            $Agrupacion->idEstadoA= $request->idEstadoA;
            $Agrupacion->idDirectiva = $request->idDirectiva;
            $Agrupacion->idTiempo=$request->idTiempo;
           
            $Agrupacion->save();

            // $Tiempo = new DuracionAgrupacion();
             
            //$Tiempo->TiempoDuracion = $request->TiempoDuracion;
            
            
           // $Tiempo->save();
         
        
             return redirect('association/associationStatus')->with('mensaje','Enviado con exito');
    }

  
    public function report(){

         $agrupacions = Agrupacion::all();
        return view('association.report', compact('agrupacions'));

    }
    public function listAssociation(){

         $agrupacions = Agrupacion::all();
        return view('association.listAssociation', compact('agrupacions'));

    }

    public function showReport( $idAgrupacion){
                
    
     $agrupacion=DB::table('Integra')
                ->join('Alumno', 'Integra.idAlumno','=','Alumno.idAlumno')
                ->join('Agrupacion','Integra.idAgrupacion','=','Agrupacion.idAgrupacion')
                ->join ('Directiva', 'Agrupacion.idDirectiva', '=','Directiva.idDirectiva')
                ->join('DuracionAgrupacion', 'Agrupacion.idTiempo', '=', 'DuracionAgrupacion.idTiempo')
                ->select('Agrupacion.NombreAgrupacion','Agrupacion.Descripcion','Agrupacion.ObjetivoAgrupacion','DuracionAgrupacion.TiempoDuracion', 'Directiva.NombreRepresentacion')
                ->where('Agrupacion.idAgrupacion','=',$idAgrupacion)
                ->get();


     $actividades=DB::table('Directiva')
                ->join('Agrupacion', 'Directiva.idDirectiva','=','Agrupacion.idDirectiva')
                ->join('Actividades','Directiva.idDirectiva','=','Actividades.idDirectiva')
                ->select('Actividades.Nombre', 'Actividades.Fecha', 'Actividades.ObjetivoActividad')
                ->where('idAgrupacion','=',$idAgrupacion)
                 ->get();

     $directiva=DB::table('Integra')
                    ->join('Agrupacion','Integra.idAgrupacion','=','Agrupacion.idAgrupacion')
                    ->join('Alumno', 'Integra.idAlumno','=','Alumno.idAlumno')
                    ->join('Cargo', 'Alumno.idAlumno', '=', 'Cargo.idAlumno')
                    ->join ('Estudia', 'Alumno.idAlumno', '=', 'Estudia.idAlumno')
                    ->join('Carrera', 'Estudia.idCarrera', '=', 'Carrera.idCarrera')
                    ->join('rolDirectiva', 'Cargo.idRol', '=', 'rolDirectiva.idRol')
                    ->select('Alumno.Nombre', 'Alumno.ApellidoPaterno', 'Alumno.ApellidoMaterno','Alumno.Email','Alumno.Celular', 'Alumno.AñoIngreso', 'Carrera.NombreCarrera', 'rolDirectiva.NombreRol')
                    ->where('Agrupacion.idAgrupacion','=',$idAgrupacion)
                    ->get();
                
                return view('association.showReport', compact('agrupacion', $agrupacion, 'actividades', $actividades, 'directiva', $directiva));

    }
    public function memberAssociation( $idAgrupacion){
                
    

     $directiva=DB::table('Integra')
                    ->join('Agrupacion','Integra.idAgrupacion','=','Agrupacion.idAgrupacion')
                    ->join('Alumno', 'Integra.idAlumno','=','Alumno.idAlumno')
                    ->join('Cargo', 'Alumno.idAlumno', '=', 'Cargo.idAlumno')
                    ->join ('Estudia', 'Alumno.idAlumno', '=', 'Estudia.idAlumno')
                    ->join('Carrera', 'Estudia.idCarrera', '=', 'Carrera.idCarrera')
                    ->join('rolDirectiva', 'Cargo.idRol', '=', 'rolDirectiva.idRol')
                    ->select('Alumno.Nombre', 'Alumno.ApellidoPaterno', 'Alumno.ApellidoMaterno','Alumno.Email','Alumno.Celular', 'Alumno.AñoIngreso', 'Carrera.NombreCarrera', 'rolDirectiva.NombreRol')
                    ->where('Agrupacion.idAgrupacion','=',$idAgrupacion)
                    ->get();
                
                return view('association.memberAssociation', compact('directiva', $directiva));

    }




    public function readAssociation(){
        $agrupacion = Agrupacion::all();
        $data['data'] = $agrupacion;
        return $data;
    }

    public function deleteAssociation($idAgrupacion){
        $agrupacion = Agrupacion::findOrFail($idAgrupacion);
        $fechaCreacion = Carbon::parse($agrupacion->created_at);
            //dd($fechaCreacion);
        $fechaActual = Carbon::now();
            //dd($fechaActual);
         $resta = $fechaCreacion->diffInDays($fechaActual);
             if ($resta<7){
         $agrupacion->delete();
         
             return redirect()->back();
         } 
            else 
            {
                return redirect('association/associationStatus')->with('mensaje','No puedes eliminar la Agrupacion, tiempo expirado');
            }
    }


    public function updateAs (Request $request){

         $rules = [
        
        'Descripcion' => 'required|min:20|max:150',
        'ObjetivoAgrupacion' => 'required|min:20|max:1000',
        
    ];

    
         $messages = [
    'NombreAgrupacion.required' => 'Debes agregar un nombre a la agrupacion',
    'NombreAgrupacion.min' =>'El nombre de la agrupacion debe ser mayor a :min caracteres.',
    'NombreAgrupacion.max' =>'El nombre de la agrupacion debe ser nemor a :max caracteres.',
    'Descripcion.required' => 'Debes agregar la descripcion de la agrupacion.',
    'Descripcion.min' => 'La descripcion de la agrupacion debe ser mayor a :min caracteres.',
    'Descripcion.max' => 'La descripcion de la agrupacion debe ser menor a :max caracteres.',
    'ObjetivoAgrupacion.required' => 'Debes agregar el objetivo de la agrupacion',
    'ObjetivoAgrupacion.min' =>'El objetivo de la agrupacion debe ser mayor a :min caracteres.',
    'ObjetivoAgrupacion.max' =>'El objetivo de la agrupacion debe ser menor a :max caracteres.',
    
];
 
    $this->validate($request, $rules, $messages);

            //dd($request);
            $agrupacion =  Agrupacion::findOrFail($request->idAgrupacion);
            
            //dd($agrupacion);
            
            $agrupacion->NombreAgrupacion = $request->NombreAgrupacion;
            $agrupacion ->Descripcion = $request->Descripcion;
            $agrupacion->ObjetivoAgrupacion =$request->ObjetivoAgrupacion;
            
            $fechaCreacion = Carbon::parse($agrupacion->created_at);
            //dd($fechaCreacion);
            $fechaActual = Carbon::now();
            //dd($fechaActual);



            $resta = $fechaCreacion->diffInDays($fechaActual);
            
            if ($resta<7){
            $agrupacion->save();

            return redirect('association/associationStatus');
            } 
            else 
            {
                return redirect('association/associationStatus')->with('mensaje','No puedes modificar la Agrupacion, tiempo expirado');
            }

    }

    public function formEdit ($idAgrupacion){
        $agrupacion = Agrupacion::findOrFail($idAgrupacion);
        return view('association.formEdit', compact('agrupacion', $agrupacion));
    }


    public function viewConsult(){

         $consult=DB::table('Agrupacion')
                  ->join('EstadoAgrupacion','Agrupacion.idEstadoA','=','EstadoAgrupacion.idEstadoA')
                  ->join('DuracionAgrupacion', 'Agrupacion.idTiempo','=','Agrupacion.idTiempo')
                  ->select('Agrupacion.NombreAgrupacion', 'Agrupacion.Descripcion', 'EstadoAgrupacion.NombreEstadoA', 'DuracionAgrupacion.TiempoDuracion')
                  ->get();
                 // dd($consult);
                  return view('association.consultAssociations', compact('consult', $consult));
    }


    public function consultAssociations(){
        
        $consult=DB::table('Agrupacion')
                  ->join('EstadoAgrupacion','Agrupacion.idEstadoA','=','EstadoAgrupacion.idEstadoA')
                  ->join('DuracionAgrupacion', 'Agrupacion.idTiempo','=','Agrupacion.idTiempo')
                  ->select('Agrupacion.NombreAgrupacion', 'Agrupacion.Descripcion', 'EstadoAgrupacion.NombreEstadoA', 'DuracionAgrupacion.TiempoDuracion')
                  ->get();
                // dd($consult);
                  return view('association.consultAssociations', compact('consult', $consult));
    }

   
}
