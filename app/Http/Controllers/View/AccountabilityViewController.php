<?php

namespace App\Http\Controllers\View;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use App\Alumno;
use App\RendicionCuenta;
use App\Directiva;
use App\Revisa;
use Illuminate\Support\Facades\Storage;



class AccountabilityViewController extends Controller
{
    //Ver vistas
    public function accountability(){
        $directiva = Directiva::all();
        return view('accountability.Accountability', compact('directiva'));
    }


    public function accountabilityHistory(){
        $rendicion=DB::table('RendicionCuentas')
              ->join('Directiva','RendicionCuentas.idDirectiva','=','Directiva.idDirectiva')
              ->select('RendicionCuentas.idRendicion','Directiva.NombreRepresentacion','RendicionCuentas.MontoEntregado','RendicionCuentas.FechaDeposito','RendicionCuentas.ItemGastado',)
              ->get();
              //dd($rendicion);
              return view('accountability.accountabilityHistory',compact('rendicion', $rendicion));
    }

    public function accountabilityStatus(){
        return view('accountability.accountabilityStatus');
    }

    public function readAccountability(){
         $rendicion=DB::table('RendicionCuentas')
              ->join('Directiva','RendicionCuentas.idDirectiva','=','Directiva.idDirectiva')
              ->select('RendicionCuentas.idRendicion','Directiva.NombreRepresentacion','RendicionCuentas.MontoEntregado','RendicionCuentas.FechaDeposito','RendicionCuentas.ItemGastado',)
              ->get();
              //dd($rendicion);
              return view('accountability.accountabilityHistory',compact('rendicion', $rendicion));
    }

    public function addAccountability(Request $request){
        //dd($request->all());
        //$idRendicion= uniqid();
        $rendicion = new RendicionCuenta;
        //image
        if($request->file('file')){
            $path = Storage::disk('public')->put('image', $request->file('file'));
            $rendicion->file=asset($path);
        }

        $rendicion->MontoEntregado=$request->all()['MontoEntregado'];
        $rendicion->FechaDeposito=$request->all()['FechaDeposito'];
        $rendicion->ItemGastado=$request->all()['ItemGastado'];
        $rendicion->idDirectiva=$request->all()['idDirectiva'];
        $rendicion->save();
        //dd($rendicion);
        return view('accountability.Accountability');
    }

    public function editAccountability(PostUpdateRequest $request, $idRendicion){
        $post = RendicionCuenta::find($idRendicion);
        $post->fill(['file'=> asset($path)])->save();

        //image
        if($request->file('file')){
            $path = Storage::disk('public')->put('image', $request->file('file'));
            $post->fill(['file' => asset($path)])->save();
        }
        //tags 

            $post->tags()->sync($request->get('tags'));
            
            return redirect()->route('posts.edit',$post->idRendicion)
            ->with('info','Rendicion creada con éxito');
            
            return redirect()->route('posts.edit, $post->idRendicion')
            ->with('info', 'Rendicion creada con éxito');
    }       

    public function readStatus(){
        $status = Revisa::all();
        $data['data'] = $status;
        return $data;
    }

    public function deleteAccountability($idRendicion){
        $accountability= RendicionCuenta::findOrFail($idRendicion);
        $accountability->delete();
            return redirect('accountability.Accountability');
    }


}
