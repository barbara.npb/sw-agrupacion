<?php

namespace App\Http\Controllers\View;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Alumno;
use App\Administradores;
use App\Carrera;
use App\EstadoAlumno;
use App\Facultad;


class MaintainerViewController extends Controller
{
    //
    public function viewAdmin(){
        return view('maintainer.administrador');
    }

     public function viewCarrera(){
        return view('maintainer.carrera');
    }
     public function ViewAlumno(){
        return view('maintainer.alumno');
    }
    public function ViewEstado(){
        return view('maintainer.estado');
    }

    public function storeStudent(Request $request){
        //dd($alumno);
        $alumno= new Alumno; 
        $alumno->rutAlumno=$request->all()['rutAlumno'];
        $alumno->idEstado=$request->all()['idEstado'];
        $alumno->Nombre=$request->all()['Nombre'];
        $alumno->ApellidoPaterno = $request->all()['ApellidoPaterno'];
        $alumno->ApellidoMaterno = $request->all()['ApellidoMaterno'];
        $alumno->Email = $request->all()['Email'];
        $alumno->Celular = $request->all()['Celular'];
        $alumno->DireccionAcademica = $request->all()['DireccionAcademica'];
        $alumno->AñoIngreso = $request->all()['AñoIngreso'];
        $alumno->save();
  
        return ['true' => 1];
    }
      public function statusTypeData(){
        $data = DB::table('EstadoAlumno')->get();
        return $data;
    } 

    public function readStudent(){
        $alumno = Alumno::all();
        $data['data'] = $alumno;
        return $data;
    }

     public function readAdmin(){
        $administrador = Administradores::all();
        $data['data'] = $administrador;
        return $data;
    } 

     public function readCarrera(){
        $carrera = Carrera::all();
        $data['data'] = $carrera;
        return $data;
    }
    
      public function readEstado(){
        $estado = EstadoAlumno::all();
        $data['data'] = $estado;
        return $data;
    }


}
