<?php

namespace App\Http\Controllers\View;

use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
//use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use App\Permission;
use App\SolicitudDinero;
use App\Directiva;
use App\Verifica;
use App\Http\Requests\dineroRequest;

class MoneyrequestViewController extends Controller{
    //ver vistas

    public function requestMoney(){
        return view('requestMoney.requestForMoney');
    }

      public function requestHistory(){
        $solicitudDinero= SolicitudDinero::all();
        return view('requestMoney.requestHistory',compact('solicitudDinero'));
    }

    public function requestStatus(){
        return view('requestMoney.requestStatus');
    }


   
        
    public function addSolicitud(Request $request){

          $rules = [
        'Monto' => 'required|integer',
        'Cotizacion' => 'required|integer',
        'Fecha' => 'required',
        'idDirectiva' => 'required',

    ];

             $messages = [
    'Monto.required' => 'Debes agregar el monto que deseas solicitar',
    'Monto.integer' =>'El monto debe ser un número entero',
    'Cotizacion.required' =>'Debes agregar la cotización correspondiente',
    'Cotizacion.integer' => 'La cotización debe ser un número entero',
    'Fecha.required' => 'Debes agregar la fecha',
    'idDirectiva.required' => 'Debes agregar la agrupación a la que perteneces',
    
];


    $this->validate($request, $rules, $messages );

        $MoneyRequest = new SolicitudDinero();
        $MoneyRequest->Monto = $request->Monto;
        $MoneyRequest ->Cotizacion = $request->Cotizacion;
        $MoneyRequest ->Fecha = $request->Fecha;
        $MoneyRequest->idDirectiva = $request->idDirectiva;
        $MoneyRequest->inserDoc = $request->inserDoc;

        $MoneyRequest->save();
        $request->session()->flash('alert-success', 'exito');
        return redirect('requestmoney/history')->with('mensaje','Enviado con éxito');
        
         //return redirect('requestmoney/requestmoney');
    }

    public function readRequest(){
        $solicitudDinero= SolicitudDinero::all();
        $data['data'] = $solicitudDinero;
        return $data;
   
         return redirect('requestmoney/requestmoney');
    }
    

    public function readEstados(){
        $estado = Verifica::all();
        $data['data'] = $estado;
        return $data;
    }
        public function destroy($idSolicitud){

        $solicitudDinero=solicitudDinero::find($idSolicitud);
        $solicitudDinero->delete();
        return redirect('requestmoney/history');
    }
        public function update($idSolicitud){

        $consulta = \DB::table('SolicitudDinero')->where('idSolicitud',$idSolicitud)->get();
        
        
        return view('requestMoney/requestEdit',compact('consulta'));
    }
     public function updateP(Request $request){

       $solicitudDinero=solicitudDinero::find($request->idSolicitud);
       $solicitudDinero->update([
            'Monto'    => $request->montoE,
            'Cotizacion' => $request->CotizacionE,            
            'Fecha'          => $request->FechaE,
            
        ]);
      // $solicitudDinero= SolicitudDinero::all();
        return view('requestMoney.requestHistory',compact('solicitudDinero'));
    }

}