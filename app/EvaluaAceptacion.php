<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluaAceptacion extends Model
{
    protected $table = 'EvaluaAceptacion';
    protected $primaryKey = 'idEvaluaAceptacion';
    public $timestamps = false; 
    protected $fillable = array(

        'idEvaluaAceptacion',
        'idAdmin',
        'idAgrupacion',

    );
    public function administrador(){

        return $this->belongsTo('App\Administrador');
    }
    public function agrupacion(){

        return $this->belongsTo('App\Agrupacion');
    }
}
