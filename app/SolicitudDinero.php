<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudDinero extends Model
{
    protected $table = 'SolicitudDinero';
    protected $primaryKey = 'idSolicitud';
    public $timestamps = false; 
    protected $fillable = array(

        'idSolicitud',
        'Monto',
        'Cotizacion',
        'Fecha',
        'idDirectiva',
        'inserDoc',

    );
     public function directiva(){

        return $this->belongsTo('App\Directiva');
    }
     public function verificas(){

        return $this->hasMany('App\Verifica');
    }
}
