@extends('layouts.app')
@section('contenido')

<h3 class="title-5 m-b-35">Asociaciones</h3> 
 		<a href="{{route('consultAssociations')}}">
 		</a>
    <div class="table-data__tool">
        <div class="table-data__tool-left">
            <div class="rs-select2--light rs-select2--md">
             </div>
        </div>
    </div>
        <div class="table-responsive table-responsive-data2">
            <table id="tableAssociacion" class="table table-data2">
                <thead>
                    <tr>
                        <th>Nombre </th>
                        <th>Descripción</th>
                        <th>Estado</th>
                        <th>Duración</th>
                    </tr>
                </thead>
                <tbody>
                 @foreach($consult as $consults)

                	<tr>
                		<td>
                			{{$consults->NombreAgrupacion}}
                		</td>
                		<td> 
                			{{$consults->Descripcion}}
                		</td>
                		<td> 
                			{{$consults->NombreEstadoA}}
                		</td>
                		<td> 
                			{{$consults->TiempoDuracion}}
                		</td>
                	</tr>
                	  @endforeach
                	</tbody>
            </table>
        </div>  
@endsection

@section('scriptFooter')
<script type="text/javascript">

</script>
@endsection