@extends('layouts.app')
@section('contenido')



   <h3 class="title-5 m-b-35">
    Asociaciones
    <a href="{{route('report')}}"></a>
   </h3> 
                               
                                <div class="table-responsive table-responsive-data2">
                                    <table id="reportA" class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th class="hidden">ID</th>
                                                <th>Nombre Agrupacion</th>
                                                <th>Detalles</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($agrupacions as $agrupacion)
                                            <tr>
                                                <td class="hidden">{{$agrupacion->idAgrupacion}}</td>
                                                <td><strong>{{$agrupacion->NombreAgrupacion}}</strong></td>
                                                <td> 
                                                    <a href=" {{route ('showReport', $agrupacion->idAgrupacion)}} " class="btn btn-primary btn-sm">Ver detalles</a>

                                                </td>
                                                  <td> 
                                                    <a href=" {{route ('memberAssociation', $agrupacion->idAgrupacion)}} " class="btn btn-primary btn-sm">Ver miembros</a>

                                                </td>

                                            </tr>


                                            @endforeach

                                    </tbody>
                                    </table>
                                </div>            

@endsection