@extends('layouts.app')
@section('contenido')


                
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header"> {{ $agrupacion[0]->NombreAgrupacion}} </div>
                                    <div class="card-body">

                                         <div class="card-title">
                                            <h3 class="text-center title-2">Datos generales de la asociacion</h3>
                                        </div>
                                        <hr>
                                  
                                 <div class="row form-group">
                                   <div class="col-lg-2">
                                                    <strong><label for="text-input" >Nombre: </label></strong>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label for="text-input"  >{{ $agrupacion[0]->NombreAgrupacion}}</label>
                                                </div>
                                               
                                                <div class="col-lg-2">
                                                   <strong> <label for="text-input" >Descripcion: </label></strong>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label for="text-input"  >{{ $agrupacion[0]->Descripcion}}</label>
                                                </div>
                                            </div>
                                            
                                            

                                  <div class="row form-group">
                                      <div class="col-lg-2">
                                                    <strong><label for="text-input" class=" form-control-label">Objetivo: </label></strong>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label for="text-input" class=" form-control-label" >{{ $agrupacion[0]->ObjetivoAgrupacion}}</label>
                                                </div>
                                                

                                                <div class="col-lg-2">
                                                   <strong> <label for="text-input" >Tiempo de duracion: </label></strong>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label for="text-input"  >{{ $agrupacion[0]->TiempoDuracion}}</label>
                                                </div>
                                                 
                                                
                                            </div>
                                             <hr>
                                              <div class="card-title">
                                            <h3 class="text-center title-2">Actividades de la asociacion </h3>
                                        </div>
                                        <hr>

                                            @foreach ($actividades as $actividad) 

                                            <div class="row form-group">
                                                
                                      <div class="col-lg-2">
                                                    <strong><label for="text-input" class=" form-control-label">Actividad: </label></strong>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label for="text-input" class=" form-control-label" >{{ $actividad->Nombre}}</label>
                                                </div>

                                                <div class="col-lg-3">
                                                    <strong><label for="text-input" class=" form-control-label">Fecha a realizar: </label></strong>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label for="text-input" class=" form-control-label" >{{ $actividad->Fecha}}</label>
                                                </div>
                                                </div>
                                            <div class="row form-group">

                                                <div class="col-lg-2">
                                                    <strong><label for="text-input" class=" form-control-label">Objetivo: </label></strong>
                                                </div>
                                                <div class="col-lg-10">
                                                    <label for="text-input" class=" form-control-label" >{{ $actividad->ObjetivoActividad}}</label>
                                                </div>
                                            </div>
                                             <br><br>
                                        
                                                @endforeach
                                          <hr>
                                              <div class="card-title">
                                            <h3 class="text-center title-2">Directiva de la asociacion </h3>
                                        </div>
                                        <hr>

                                      @foreach ($directiva as $directivas)
                                      <hr>

                                        <div class="card-title">
                                            <h6 class="text-center title-2">{{ $directivas->NombreRol}} </h6>
                                        </div>
                                        <hr>

                                         <div class="row form-group">
                                                
                                                <div class="col-lg-2">
                                                    <strong><label for="text-input" class=" form-control-label">Nombre: </label></strong>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label for="text-input" class=" form-control-label" >{{ $directivas->Nombre}} {{ $directivas->ApellidoPaterno}} {{ $directivas->ApellidoMaterno}}</label>
                                                </div>

                                                <div class="col-lg-2">
                                                    <strong><label for="text-input" class=" form-control-label">Carrera: </label></strong>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label for="text-input" class=" form-control-label" >{{ $directivas->NombreCarrera}}
                                                    </p></label>
                                                </div>
                                         </div>

                                         <div class="row form-group">
                                                
                                                <div class="col-lg-2">
                                                    <strong><label for="text-input" class=" form-control-label">Año de ingreso: </label></strong>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label for="text-input" class=" form-control-label" >{{ $directivas->AñoIngreso}}</label>
                                                </div>

                                                <div class="col-lg-2">
                                                    <strong><label for="text-input" class=" form-control-label">Email: </label></strong>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label for="text-input" class=" form-control-label" >{{ $directivas->Email}}
                                                </div>
                                         </div>



                                       @endforeach



                                    </div>
                                </div>
                            </div>

                            
@endsection