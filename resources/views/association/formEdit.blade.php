@extends('layouts.app')
@section('contenido')
<div class="col-lg-9">
    <div class="card">
        <div class="card-header">
            <strong>Formulario de Postulación</strong>
        </div>
        <div class="card-body card-block">

             @if(session()->has('mensaje'))
            <div class="alert alert-success" id="alerta">
            {{ session('mensaje') }}
             </div>
             @endif

            @if (count($errors) > 0)
               <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                     @endforeach
                 </ul>
               </div>

            @endif


             <form action="{{ route ('updateAs')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{csrf_field()}}
                {{method_field('POST')}}


         <!--------------------------------------------------------------------------------------------------->
                 <div class="row form-group" align="center" >
                    <div class="col col-md-7">
                        <strong><label class=" form-control-label">Datos de la agrupacion</label></strong>
                    </div>
                 </div>
        <!--------------------------------------------------------------------------------------------------------->


                 <div class="row form-group">
                     <div class="col col-md-4">
                        <label for="text-input" class=" form-control-label">Nombre de la Agrupacion</label>
                     </div>

                     <div class="col-sm-8">
                        <input type="text" id="NombreAgrupacion" name="NombreAgrupacion" class="form-control" value="{{$agrupacion->NombreAgrupacion}}">
                        <small class="form-text text-muted">Ingrese el nombre de la agrupacion</small>

                     </div>
                 </div>



                 <div class="row form-group">
                     <div class="col col-md-4">
                        <label for="text-input" class=" form-control-label">Descripcion </label>
                     </div>

                     <div class="col-sm-8">
                        <input type="text" id="Descripcion" name="Descripcion" placeholder="Descripcion de la agrupacion" class="form-control" value="{{$agrupacion->Descripcion}}">
                        <small class="form-text text-muted">Ingrese una descripcion de la agrupacion</small>
                     </div>
                 </div>
          <!--------------------------------------------------------------------------------------------------------->

                 <div class="row form-group">
                     <div class="col col-md-4">
                        <label for="textarea-input" class=" form-control-label">Objetivo de la Agrupación</label>
                     </div>

                     <div class="col-sm-8">
                        <textarea  name="ObjetivoAgrupacion" id="ObjetivoAgrupacion" rows="9" placeholder="Describa brevemente el objetivo de la nueva agrupación" class="form-control"value="{{$agrupacion->ObjetivoAgrupacion}}"></textarea>
                     </div>
                 </div>


                 <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm" name="idAgrupacion" value="{{$agrupacion->idAgrupacion}}">
                        <i class="fa fa-dot-circle-o"></i>
                          Editar
                    </button>

                    <a href= "{{route ('associationStatus')}}"type="reset" class="btn btn-danger btn-sm" ><i class="fa fa-ban"></i>
                         Cancelar</a>

                 </div>

             </form>


        </div>
    </div>
</div>

@endsection
