@extends('layouts.app')
@section('contenido')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header">
            <strong>Rendición de cuentas</strong> Formulario      
            
        </div>
        
          	<div class="card-body card-block">
               
                
                <form action="{{Route('addAccountability')}}" id="editForm" method="post" enctype="multipart/form-data" class="form-horizontal">
                   

                    {{csrf_field()}}
                	<!-- -->

                	<div class="row form-group">
                        <div class="col col-md-3">
                            <label for="select" class=" form-control-label">Agrupación</label>
                        </div>


                            <div class="col-12 col-md-9">
                                <select name="idDirectiva" id="select" class="form-control" required>
                                    @foreach ($directiva as $directivas)
                                    <option value="{{$directivas->idDirectiva}}">{{$directivas->NombreRepresentacion}}</option>
                                    @endforeach
                                </select>
                                <small class="form-text text-muted">Ingrese la agrupación a la que pertenece</small>
                            </div>
                    </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="number-input" class=" form-control-label">Monto Entregado</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="number" id="MontoEntregado" name="MontoEntregado" placeholder="Monto total" class="form-control" required>
                                    <small class="form-text text-muted">Ingrese el monto entregado</small>
                            </div>
                        </div>
                    
                   	<!-- -->
                    <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="number-input" class=" form-control-label">Monto Gastado</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="number" id="ItemGastado" name="ItemGastado" placeholder="Monto gastado" class="form-control" required>
                                    <small class="form-text text-muted">Ingrese el monto gastado</small>
                            </div>
                        </div>
                    
                    <!-- -->
                   	<div class="row form-group">
                        <div class="col col-md-3">
                            <label for="date" class=" form-control-label">Fecha Depósito</label>
                        </div>
                            <div class="col-12 col-md-9">
                                <input type="date" name="FechaDeposito" min="2019-07-13"  placeholder="fecha" class="form-control"required>
                                    <small class="form-text text-muted">Ingrese fecha del deposito</small>
                            </div>
                    </div>
                
                    
            <!-- -->

                	<div class="row form-group">
                        <div class="col col-md-3">
                            <label for="number-input" class=" form-control-label">Adjunte documentos de respaldo</label>
                        </div>
                    <div class="col-12 col-md-9">
                        <input type="file" id="file" name="file" multiple="" class="form-control-file" required>
                            <small class="form-text text-muted">Hasta 16MB </small>
                    </div>
                </div>

                    <div class="card-footer">
                        <center> 
                            <button type= "reset" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Guardar
                            </button>     
                            <button type="submit" class="btn btn-success btn-sm">
                                <i class="far fa-envelope"></i> Enviar
                            </button>
                            

                        </center>
                    </div>
            
                </form>
            </div>


@endsection