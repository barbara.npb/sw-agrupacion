@extends('layouts.app')
@section('contenido')

                                <h3 class="title-5 m-b-35">Estado Rendición de cuentas</h3> 
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            
                                        </div>
                                       
                                    </div>
                                    <div class="table-data__tool-right">
                                        <button class="au-btn au-btn-icon au-btn--blue au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>Agregar</button>
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table id="status" class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th class="hidden">idRevisa</th>
                                                <th> Rendición</th>
                                                <th class="hidden">idAdmin</th>
                                                <th>Estado</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                                   <!-- <div class="table-data-feature">
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Send">
                                                            <i class="zmdi zmdi-mail-send"></i>
                                                        </button>
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>
                                                        </div>
                                                </td>
                                             -->
                                            
                                        </tbody>
                                    </table>
                                </div>
                                
@endsection

@section('scriptFooter')
<script type="text/javascript">

$(document).ready(function() {
        $('#status').DataTable( {
        language: {
            sProcessing:     "Cargando datos...&nbsp;&nbsp;<img height='32' width='32' src='{{url('dist/img/loader.gif')}}'>",
            sLengthMenu:     "Buscar _MENU_ registros",
            sZeroRecords:    "No se encontraron resultados",
            sEmptyTable:     "No existe ningún registro en este momento",
            sInfo:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            sInfoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
            sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",
            sInfoPostFix:    "",
            sSearch:         "Buscar:",
            sUrl:            "",
            sInfoThousands:  ",",
            sLoadingRecords: "&nbsp;",
            oPaginate: {
                sFirst:    "Primero",
                sLast:     "Último",
                sNext:     "Siguiente",
                sPrevious: "Anterior"
            },
            oAria: { //orden de datos alfabeticamente 
                sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                sSortDescending: ": Activar para ordenar la columna de manera descendente"
            }
        },
        ajax: {
            url: "{{route('readStatus')}}",
            type: "GET",
            beforeSend: function (request) {
                console.log('esooou');
                request.setRequestHeader("X-CSRF-TOKEN", $('input[name="_token"]').val());
            },
        },
                    //Employee dates: picture, name and postulating button//
                    //
                    columns: [
                    {
                        data: "idRevisa",
                        class: "hidden",
                        defaultContent:"default"
                    },
                    { 
                        data: "idRendicion",
                        
                        defaultContent: "default"
                    },
                    { 
                        data: "idAdmin", 
                        class:"hidden",
                        defaultContent: "default"
                    },
                    {
                        data: "Estado",
                        defaultContent: "default"
                    },
                    
                    ],

                     
                    paging: true,
                    lengthChange: true,
                    searching: true,
                    ordering: true,
                    info: true,
                    autoWidth: true,
                    processing: true,
                //order: [[ 12, "desc" ]],

            }); 

});

</script>
@endsection