@extends('layouts.app')
@section('contenido')

                                <h3 class="title-5 m-b-35">Historial rendición de cuentas</h3> 
                                <a href="{{route('readAccountability')}}">
                                </a>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            
                                        </div>
                                       
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table id="TableHistory" class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th class="hidden">id</th>
                                                <th>Directiva</th>
                                                <th>Monto Entregado</th>
                                                <th>Fecha Deposito</th>
                                                <th>Monto gastado </th>
                                                <th></th>
                                            </tr>  
                                        </thead>
                                        <tbody>
                                            @foreach($rendicion as $rendicions)
                                            <tr> 
                                                <td class="hidden">
                                                    {{$rendicions->idRendicion}} 
                                                </td>
                                                <td> 
                                                    {{$rendicions->NombreRepresentacion}}  
                                                </td>
                                                <td> 
                                                    {{$rendicions->MontoEntregado}} 
                                                </td>
                                                <td> 
                                                    {{$rendicions->FechaDeposito}} 
                                                </td>
                                                <td> 
                                                    {{$rendicions->ItemGastado}} 
                                                </td>
                                                
                                            </tr>
                                            @endforeach
                                        </tbody>
                                       
                                    </table>
                                </div>
                                

@endsection

@section('scriptFooter')
<script type="text/javascript">


$('#editForm').on('click', 'a.editor_edit', function (e) {

    e.preventDefault();


    swal({
        title: 'Editar Rendición',
        html:
        '<input id="idRendicion" class="swal2-input" hidden disabled value="'+$(this).closest("tr").children("td:first").text()+'">'+
        '<b>Agrupación</b>  <input id="idDirectiva" class="swal2-input" value="'+$(this).closest("tr").children("td:nth-child(2)").text()+'">'+
        '<b>Monto Entregado</b>  <input id="MontoEntregado" class="swal2-input" value="'+$(this).closest("tr").children("td:nth-child(2)").text()+'">'+
        '<b>Monto Gastado</b>  <input id="ItemGastado" class="swal2-input" value="'+$(this).closest("tr").children("td:nth-child(2)").text()+'">'+
        '<b>Fecha depósito</b>  <input id="FechaDeposito" class="swal2-input" value="'+$(this).closest("tr").children("td:nth-child(2)").text()+'">'+
        '<b>Documentos</b>  <input id="inserDoc" class="swal2-input" value="'+$(this).closest("tr").children("td:nth-child(2)").text()+'">',

        focusConfirm: false,
        showCancelButton: true,
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#91DF38',
        confirmButtonText: 'Confirmar',
        reverseButtons: false,

        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                setTimeout(function() {
                    resolve([
                        $('#idRendicion').val(),
                        $('#idDirectiva').val(),
                        $('#MontoEntregado').val(),
                        $('#ItemGastado').val(),
                        $('#FechaDeposito').val(),
                        $('#inserDoc').val()
                        ])
                }, 2000)

            })
        }
    }).then(function (result){

        console.log(result);

        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
           url: '{{route('editAccountability')}}',
            type: 'PUT',
            dataType: 'JSON',
            data: {
                idRendicion:result[0], 
                idDirectiva:result[1],
                MontoEntregado:result[2],
                ItemGastado:result[3],
                FechaDeposito:result[4],
                inserDoc:result[5],
                active:1
            },

            beforeSend: function () {

                swal({
                    title: 'Espere...',
                    text: 'Editando Rendición de cuentas',
                    onOpen: () => {
                        swal.showLoading()
                    }
                }).then((result) => {
                    if (result.dismiss === 'timer') {
                    }
                })
            },
        }).done(function( data ) {
            swal(
                '¡Enviado!',
                'La Rendición ha sido actualizado con exito',
                'success'
                ).then(function () {
                    location.reload();
                })
            }).fail(function(data) {
                swal(
                    '¡Error!',
                    'La Rendición no pudo ser actualizada, favor intente más tarde',
                    'error'
                    )
            });

        }).catch(swal.noop)
});


</script>
@endsection