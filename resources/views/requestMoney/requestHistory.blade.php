@extends('layouts.app')
@section('contenido')

                               <h3 class="title-5 m-b-35">Historial de Solicitudes</h3> 
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            
                                        </div>
                                       
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2" >
                                   <table class="table table-hover">
    <thead>
        <tr>
            
            <th class="text-center">Monto</th>
            <th class="text-center">Cotizacion</th>
            <th class="text-center">Fecha</th>
        </tr>
    </thead>
    <tbody>



        @foreach ($solicitudDinero as $element)
        
        <tr id="{{ $element->idSolicitud }}" >

            
            <td class="text-center">{{ $element->Monto }}</td>
            <td class="text-center">{{ $element->Cotizacion }}</td>
            <td class="text-center">{{ $element->Fecha }}</td>
            
            <th class="text-center"><a class="btnEliminar" href="{{route('destroy',$element->idSolicitud )}}"><i class="fas fa-trash-alt"></a></th>
                <th class="text-center"><a class="btnEditar" href="{{route('update',$element->idSolicitud )}}"><i class="fas fa-edit"></a></th>
        </tr>
        @endforeach
    </tbody>
</table>
                                </div>
                                

@endsection