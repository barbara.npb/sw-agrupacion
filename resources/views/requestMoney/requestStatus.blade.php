@extends('layouts.app')
@section('contenido')

                                <h3 class="title-5 m-b-35">Estado Solicitud de dinero</h3> 
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            
                                        </div>
                                       
                                    </div>
                                   <!-- <div class="table-data__tool-right">
                                        <button class="au-btn au-btn-icon au-btn--blue au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>Agregar</button>
                                    </div> -->
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table id="estado" class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th class="hidden">idVerifica</th>
                                                <th class="hidden">idAdmin</th>
                                                <th> Solicitud</th>
                                                <th>Estado</th>
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                                

@endsection
@section('scriptFooter')
<script type="text/javascript">

$(document).ready(function() {
        $('#estado').DataTable( {
        language: {
            sProcessing:     "Cargando datos...&nbsp;&nbsp;<img height='32' width='32' src='{{url('dist/img/loader.gif')}}'>",
            sLengthMenu:     "Buscar MENU registros",
            sZeroRecords:    "No se encontraron resultados",
            sEmptyTable:     "No existe ningún registro en este momento",
            sInfo:           "Mostrando registros del START al END de un total de TOTAL registros",
            sInfoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
            sInfoFiltered:   "(filtrado de un total de MAX registros)",
            sInfoPostFix:    "",
            sSearch:         "Buscar:",
            sUrl:            "",
            sInfoThousands:  ",",
            sLoadingRecords: "&nbsp;",
            oPaginate: {
                sFirst:    "Primero",
                sLast:     "Último",
                sNext:     "Siguiente",
                sPrevious: "Anterior"
            },
            oAria: { //orden de datos alfabeticamente 
                sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                sSortDescending: ": Activar para ordenar la columna de manera descendente"
            }
        },
        ajax: {
            url: "{{route('readEstados')}}",
            type: "GET",
            beforeSend: function (request) {
                console.log('esooou');
                request.setRequestHeader("X-CSRF-TOKEN", $('input[name="_token"]').val());
            },
        },
                    //Employee dates: picture, name and postulating button//
                    //
                    columns: [
                    {
                        data: "idVerifica",
                        class: "hidden",
                        defaultContent:"default"
                    },
                    { 
                        data: "idAdmin",
                        class:"hidden",
                        defaultContent: "default"
                    },
                    { 
                        data: "idSolicitud", 

                        defaultContent: "default"
                    },
                    {
                        data: "Estado",
                        defaultContent: "default"
                    },
                    
                    ],

                     
                    paging: true,
                    lengthChange: true,
                    searching: true,
                    ordering: true,
                    info: true,
                    autoWidth: true,
                    processing: true,
                //order: [[ 12, "desc" ]],

            }); 

});

</script>
@endsection