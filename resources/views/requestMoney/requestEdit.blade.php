@extends('layouts.app')
@section('contenido')

<h3 class="title-5 m-b-35">Historial de Solicitudes/Editar</h3> 
{{csrf_field() }}

<form action="{{route('updateP')}}" method="post" enctype="multipart/form-data" >
    @foreach ($consulta as $element)


<input type="hidden" value="{{ $element->idSolicitud }}" id="idSolicitud" name="idSolicitud">

<label>Monto:</label>
<input type="number" class="form-control" required="" name="montoE" id="montoE" value="{{ $element->Monto }}">
<label>Cotizacion:</label>
<input type="number" class="form-control" required="" name="CotizacionE" id="CotizacionE" value="{{ $element->Cotizacion }}">
<label>Fecha:</label>
<input type="date" class="form-control" required="" name="FechaE" id="FechaE" value="{{ $element->Fecha }}">


                       

<div>
<button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Enviar
        </button>  
        </div> 
@endforeach

</form>                         
                               
 @endsection