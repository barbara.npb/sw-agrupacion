@extends('layouts.app')
@section('contenido')


<div class="col-lg-6">
    <div class="card">
        <div class="card-header">
            <strong>Solicitud Dinero</strong> Formulario
        </div>

            <div class="card-body card-block">



                @if(session()->has('mensaje'))
            <div class="alert alert-success" id="alerta">
            {{ session('mensaje') }}
             </div>
             @endif

            @if (count($errors) > 0)
               <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                     @endforeach
                 </ul>
               </div>

            @endif

            



                <form action="{{ route('addSolicitud') }} " method="post" enctype="multipart/form-data" class="form-horizontal">
                    <!-- -->{{csrf_field() }}


                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="select" class=" form-control-label">Directiva</label>
                        </div>
                            <div class="col-12 col-md-9">
                                <select name="idDirectiva" id="idDirectiva" class="form-control">
                                    <option value="">Seleccione directiva</option>
                                    <option value="41">Mapau</option>
                                    <option value="42">Ajedrez</option>
                                    <option value="43">Basquetball</option>
                                    <option value="44">Jamaicana</option>
                                    <option value="45">Lectores</option>
                                </select>
                                <small class="form-text text-muted">Ingrese la directiva a la que pertenece</small>

                            
                             </div>
                        </div>
<!------------------------------------------------------------------------------------------------------------->
                   <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Actividad a realizar</label>
                        </div>
                            <div class="col-12 col-md-9">
                              <select name="idActividad" id="idActividad" class="form-control">
                                    <option value="0">Seleccione Actividad</option>
                                    <option value="126">Aniversario N2</option>
                                    <option value="127">Día de la familia</option>
                                    <option value="128">Día del chicle</option>
                                    <option value="123">Día del deporte</option>
                                    <option value="125">Día saludable</option>
                                </select>
                                    <small class="form-text text-muted">Seleccione Actividad</small>
                            </div>
                    </div> 
<!---------------------------------------------------------------------------------------------------------->
                 
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="date" class=" form-control-label">Fecha Actividad</label>
                        </div>
                            <div class="col-12 col-md-9">
                                <input type="date" name="Fecha" min="2019-08-14"  placeholder="fecha" class="form-control">
                                    <small class="form-text text-muted">Ingrese fecha de la actividad para la que se utilizará</small>
                          

                            </div>
                    </div>
<!---------------------------------------------------------------------------------------------------------->
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="number-input" class=" form-control-label">Monto Total</label>
                        </div>
                            <div class="col-12 col-md-9">
                                <input type="number" id="Monto" name="Monto" placeholder="Monto total" class="form-control">
                                    <small class="form-text text-muted">Ingrese el monto total de la solicitud</small>
                             
                            </div>
                    </div>
<!------------------------------------------------------------------------------------------------------------>

                     <div class="row form-group">
                        <div class="col col-md-3">
                             <label for="number-input" class=" form-control-label">Cotización</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="number" id="Cotizacion" name="Cotizacion" placeholder="Cotizacion" class="form-control">
                                <small class="form-text text-muted">Ingrese cotización</small>
                               
                        </div>
                    </div>

<!------------------------------------------------------------------------------------------------------------>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="number-input" class=" form-control-label">Adjunte Presupuesto</label>
                        </div>
            <div class="col-12 col-md-9">
                <input type="file" id="inserDoc" name="inserDoc" multiple="" class="form-control-file">
                <small class="form-text text-muted">Seleccione archivo </small>
            </div>
<!------------------------------------------------------------------------------------------------------------->
            <!--Botones -->
<div class="card-footer">
    <center> 
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Enviar
        </button>     
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Cancelar
    </center>
</div>
                </form>
            </div>           

@endsection