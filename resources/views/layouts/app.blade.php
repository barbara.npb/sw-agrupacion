<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Asociaciones UBB</title>

    <!-- Fontfaces CSS-->
    <link href="{{ asset('assets/css/font-face.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/font-awesome-4.7/css/font-awesome.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/mdi-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ asset('assets/vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{ asset('assets/vendor/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/wow/animate.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/slick/slick.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/vendor/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">

    <!-- Main CSS-->

    <link href="{{ asset('assets/css/theme.css') }}" rel="stylesheet" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>

    <style type="text/css">
            .hidden {
              display: none !important;
            }
        </style>

    <link href="{{ asset('assets/css/theme.css')}}" rel="stylesheet" media="all">


</head>

<body class="animsition">
    <div class="page-wrapper">
         <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="{{route('home')}}">
                    <img src="{{ asset('assets/images/icon/logo.png')}}" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    
                    <ul class="list-unstyled navbar__list">

                        @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('user'))
                                                   
                                               
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-graduation-cap"></i>Agrupación</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="{{route('postulationAssociation')}}">Formulario de postulación</a>
                                </li>
                                <li>
                                    <a href="{{route('associationStatus')}}">Editar postulación</a>
                                </li>
                                                           
                            </ul>
                        </li>
                           

                        
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-calendar-alt"></i>Actividades</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="{{route('activities')}}">Crear Actividad</a>
                                </li>
                                
                            </ul>
                        </li>
                        


                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-dollar-sign"></i>Solicitud de dinero</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="{{route('viewRequest')}}">Crear Solicitud</a>
                                </li>
                                <li>
                                    <a href="{{route('viewHistory')}}">Editar solicitud</a>
                                </li>         
                                                     
                               
                              
                            </ul>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-dollar-sign"></i>Rendición de cuentas</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="{{route('accountability')}}">Crear rendición</a>
                                </li>
                                
                            </ul>
                        </li>
                        
                        @endif

                        @if(Auth::user()->hasRole('admin'))
                       
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-users"></i>DDE</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="{{route('activities')}}">Agrupaciones</a>
                                    <ul class="list-unstyled navbar__sub-list js-sub-list active has-sub">
                                    <li>
                                        <a href="{{route('viewConsult')}}">Ver agrupaciones </a>
                                    </li>
                                    <li>
                                    <a href="{{route('historyActiv')}}">Ver Actividades</a>
                                    </li>
                                    <li >
                                        <a href="{{route('report')}}" >Reporte de la agrupación</a>
                                    </li>

                                    <li>
                                        <a href="{{route('listAssociation')}}">Miembros </a>
                                    </li>
                                   
                                    
                                </ul>
                                </li>
                                
                                 <li>
                                    <a href="{{route('histotyAcc')}}">Historial Solicitud dinero</a>
                                </li>
                                 <li>
                                    <a href="{{route('histotyAcc')}}">Estado Solicitud dinero</a>
                                </li>
                                 <li>
                                    <a href="{{route('histotyAcc')}}">Rendición de cuentas</a>
                                </li>
                                
                            </ul>
                        </li>
                      

                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-desktop"></i>Mantenedor</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="{{route('alumno')}}">Ingresar alumno</a>
                                </li>
                                <li>
                                    <a href="{{route('carrera')}}">Ingresar carrera</a>
                                </li>
                                <li>
                                    <a href="{{route('admin')}}">Ingresar administrador</a>
                                </li>
                                <li>
                                    <a href="{{ route('verRegistro') }}">Registrar Usuario</a>
                                </li>
                               @endif
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>


        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                
                            </form>
                           
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"> {{ Auth::user()->name}}</a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                       
                                                    </a>
                                                </div>
                                            <div class="account-dropdown__item">
                                                <a href="#">
                                                    <i class="zmdi zmdi-account"></i>Mi perfil</a>
                                                      <a class="dropdown-item" href="{{ route('logout') }}">

                                        <i class="fa fa-power-off icon"></i> Salir </a>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                @yield('contenido')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <p>Copyright © 2019 Todos los derechos reservados "Las Chicas" </a>.</p>
                </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- Jquery JS-->
    <script src="{{ asset('assets/vendor/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap JS-->
    <script src="{{ asset('assets/vendor/bootstrap-4.1/popper.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-4.1/bootstrap.min.js') }}"></script>
    <!-- Vendor JS       -->
    <script src="{{ asset('assets/vendor/slick/slick.min.js') }}">
    </script>
    <script src="{{ asset('assets/vendor/wow/wow.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/animsition/animsition.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') }}">
    </script>
    <script src="{{ asset('assets/vendor/counter-up/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/counter-up/jquery.counterup.min.js') }}">
    </script>
    <script src="{{ asset('assets/vendor/circle-progress/circle-progress.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('assets/vendor/chartjs/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/select2/select2.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('dist/js/sweetalert2.min.js') }}"></script>



    <!-- Main JS-->
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
    <script src="jquery.rut.js"></script>
        

    @yield('scriptFooter')
</body>

</html>
