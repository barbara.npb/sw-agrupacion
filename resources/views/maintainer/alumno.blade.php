@extends('layouts.app)
@section('contenido')
   <h3 class="title-5 m-b-35">Alumno</h3> 
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            
                                        </div>
                                       
                                    </div>
                                    <div class="table-data__tool-right">
                                        <button id="boton_agregar" class="au-btn au-btn-icon au-btn--blue au-btn--small">
                                            <i class="zmdi zmdi-plus" ></i>Agregar</button>
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table id="tableStudent" class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th class="hidden">ID</th>
                                                <th>Rut</th>
                                                <th>Nombre</th>
                                                <th>Apellido Paterno</th>
                                                <th>Apellido Materno</th>
                                                <th>Email</th>
                                                <th>Celular</th>
                                                <th class="hidden">Dirección Académica</th>
                                                <th>Año Ingreso</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>            

@endsection

@section('scriptFooter')
<script type="text/javascript">

 //----Data table 
 


$(document).ready(function() {
        $('#tableStudent').DataTable( {
        language: {
            sProcessing:     "Cargando datos...&nbsp;&nbsp;<img height='32' width='32' src='{{url('dist/img/loader.gif')}}'>",
            sLengthMenu:     "Buscar _MENU_ registros",
            sZeroRecords:    "No se encontraron resultados",
            sEmptyTable:     "No existe ningún registro en este momento",
            sInfo:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            sInfoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
            sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",
            sInfoPostFix:    "",
            sSearch:         "Buscar:",
            sUrl:            "",
            sInfoThousands:  ",",
            sLoadingRecords: "&nbsp;",
            oPaginate: {
                sFirst:    "Primero",
                sLast:     "Último",
                sNext:     "Siguiente",
                sPrevious: "Anterior"
            },
            oAria: { //orden de datos alfabeticamente 
                sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                sSortDescending: ": Activar para ordenar la columna de manera descendente"
            }
        },
        ajax: {
            url: "{{route('readStudent')}}",
            type: "GET",
            beforeSend: function (request) {
                console.log('H');
                request.setRequestHeader("X-CSRF-TOKEN", $('input[name="_token"]').val());
            },
        },
                    //Employee dates: picture, name and postulating button//
                    //
                    columns: [
                    {
                        data: "idAlumno",
                        class: "hidden",
                        defaultContent:"default"
                    },
                    { 
                        data: "rutAlumno",
                        
                        defaultContent: "default"
                    },
                    { 
                        data: "Nombre", 
                        defaultContent: "default"
                    },
                    {
                        data: "ApellidoPaterno",
                        defaultContent: "default"
                    },
                    {
                        data: "ApellidoMaterno",
                        defaultContent: "default"
                    },
                    {
                        data: "Email",
                        defaultContent: "default"
                    },
                    {
                        data:"Celular",
                        defaultContent: "default"
                    },
                    {
                        data:"DireccionAcademica",
                        class:"hidden",
                        defaultContent: "default"
                    },
                    {
                        data:"AñoIngreso",
                        defaultContent: "default"
                    },

                    ],

                     
                    paging: true,
                    lengthChange: true,
                    searching: true,
                    ordering: true,
                    info: true,
                    autoWidth: true,
                    processing: true,
                //order: [[ 12, "desc" ]],

            }); 

});

//-----Boton añadir alumno 

  $('#boton_agregar').on('click', function (e) {

    e.preventDefault();

    swal({
        title: 'Añadir',
        html:
        
        '<b>Rut</b>  <input .rut() name="rutAlumno" id="rutAlumno" class="swal2-input" $("form#basico input").rut();>'+
        '<b>Estado Alumno</b> <select id="idEstado" class="swal2-input"><option value="101">Regular</option><option value="102">Terminal</option><option value="103">Egresado</option><option value="104">Titulado</option><option value="105">Pasante</option></select>'+
        '<b>Nombre</b>  <input id="Nombre" class="swal2-input" >'+
        '<b>Apellido Paterno</b>  <input id="ApellidoPaterno" class="swal2-input" >'+
        '<b>Apellido Materno</b>  <input id="ApellidoMaterno" class="swal2-input" >'+
        '<b>Email</b>  <input id="Email" class="swal2-input" >'+
        '<b>Celular</b>  <input id="Celular" class="swal2-input" >'+
        '<b>Dirección Académica</b>  <input id="DireccionAcademica" class="swal2-input" >'+
        '<b>Año Ingreso</b>  <input id="AñoIngreso" class="swal2-input" >',


        focusConfirm: false,
        showCancelButton: true,
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#91DF38',
        confirmButtonText: 'Confirmar',
        reverseButtons: false,

        preConfirm: function () {                       //rescata los datos de los input
            return new Promise(function (resolve, reject) {
                setTimeout(function() {
                    resolve([
                        $('#rutAlumno').val(),
                        $('#idEstado').val(),
                        $('#Nombre').val(),
                        $('#ApellidoPaterno').val(),
                        $('#ApellidoMaterno').val(),   
                        $('#Email').val(),
                        $('#Celular').val(),
                        $('#DireccionAcademica').val(), 
                        $('#AñoIngreso').val()             //recupero el valor del input
                        ])
                }, 2000)

            })          
        }
    }).then(function (result){

     console.log(result);

     $.ajax({
        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
        url: '{{route('storeStudent')}}',
        type: 'POST', //
        dataType: 'JSON',
        data: { 
            rutAlumno:result[0],
            idEstado:result[1],
            Nombre:result[2],
            ApellidoPaterno:result[3],
            ApellidoMaterno:result[4],
            Email:result[5],
            Celular:result[6],
            DireccionAcademica:result[7],
            AñoIngreso:result[8],
            Active:1
        },
        beforeSend: function () {

            swal({
                title: 'Espere...',
                text: 'Registrando Alumno',
                onOpen: () => {
                    swal.showLoading()
                }
            }).then((result) => {
                if (result.dismiss === 'timer') {
                }
            })
        },
    }).done(function( data ) {

      console.log(data);

      swal(
        '¡Enviado!',
        'El Alumno ha sido agregado con exito',
        'success'
        ).then(function () {
            location.reload();
        })
    }).fail(function(data) {
        swal(
            '¡Error!',
            'El Alumno no pudo ser agregado, ingrese un rut válido',
            'error'
            )
    });


}).catch(swal.noop)
});

$(function() {
    $("#rutAlumno").rut().on('rutValido', function(e, rut, dv) {
        alert("El rut " + rutAlumno + "-" + dv + " es correcto");
    }, { minimumLength: 7} );
})

</script>
@endsection
