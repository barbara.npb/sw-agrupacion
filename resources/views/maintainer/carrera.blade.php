@extends('layouts.app')
@section('contenido')
   <h3 class="title-5 m-b-35">Carreras Universitarias</h3> 
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            
                                        </div>
                                       
                                    </div>
                                   <!-- <div class="table-data__tool-right">
                                        <button class="au-btn au-btn-icon au-btn--blue au-btn--small">
                                            <i class="zmdi zmdi-plus" id="boton_agregar"></i>Agregar</button>
                                    </div> -->
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table id="tableCarrera" class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th class="hidden">ID</th>
                                                <th>Código Carrera</th>
                                                <th class="hidden">ID</th>
                                                <th>Nombre Carrera</th>
                                                <th>Semestres Duración</th>
                                                
                                                
                                            </tr>
                                           
                                        </thead>
                                    </table>
                                </div>            

@endsection

@section('scriptFooter')
<script type="text/javascript">

 //----Data table 
 


$(document).ready(function() {
        $('#tableCarrera').DataTable( {
        language: {
            sProcessing:     "Cargando datos...&nbsp;&nbsp;<img height='32' width='32' src='{{url('dist/img/loader.gif')}}'>",
            sLengthMenu:     "Buscar _MENU_ registros",
            sZeroRecords:    "No se encontraron resultados",
            sEmptyTable:     "No existe ningún registro en este momento",
            sInfo:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            sInfoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
            sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",
            sInfoPostFix:    "",
            sSearch:         "Buscar:",
            sUrl:            "",
            sInfoThousands:  ",",
            sLoadingRecords: "&nbsp;",
            oPaginate: {
                sFirst:    "Primero",
                sLast:     "Último",
                sNext:     "Siguiente",
                sPrevious: "Anterior"
            },
            oAria: { //orden de datos alfabeticamente 
                sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                sSortDescending: ": Activar para ordenar la columna de manera descendente"

            }

        },

        ajax: {
            url: "{{route('readCarrera')}}",
            type: "GET",

            beforeSend: function (request) {
                console.log('aquictm');
                request.setRequestHeader("X-CSRF-TOKEN", $('input[name="_token"]').val());
            },
        },
                    //Employee dates: picture, name and postulating button//
                    //
                    columns: [
                    
                    { 
                        data: "idCarrera",
                        class: "hidden",
                        defaultContent: "default"
                    },
                    {
                        data: "CodigoCarrera",
                        defaultContent: "default"
                    },
                    {
                        data: "idFacultad",
                        class: "hidden",
                        defaultContent: "default"
                    },
                    { 
                        data: "NombreCarrera", 
                        defaultContent: "default"
                    },
                    { 
                        data: "SemestresDuracion", 
                        defaultContent: "default"
                    },
                    
                    
                    ],

                     
                    paging: true,
                    lengthChange: true,
                    searching: true,
                    ordering: true,
                    info: true,
                    autoWidth: true,
                    processing: true,
                //order: [[ 12, "desc" ]],

            }); 

});

$('#boton_agregar').on('click', function (e) {

    e.preventDefault();

    swal({
        title: 'Añadir',
        html:
        
        '<b>Rut</b>  <input id="rutAlumno" class="swal2-input" >'+
        '<b>Estado Alumno</b> <select id="idEstado" class="swal2-input"><option value="101">Regular</option><option value="102">Terminal</option><option value="103">Egresado</option><option value="104">Titulado</option><option value="105">Pasante</option></select>'+
        '<b>Nombre</b>  <input id="Nombre" class="swal2-input" >',


        focusConfirm: false,
        showCancelButton: true,
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#91DF38',
        confirmButtonText: 'Confirmar',
        reverseButtons: false,

        preConfirm: function () {                       //rescata los datos de los input
            return new Promise(function (resolve, reject) {
                setTimeout(function() {
                    resolve([
                        $('#rutAlumno').val(),
                        $('#idEstado').val(),
                        $('#Nombre').val(),
                        $('#ApellidoPaterno').val(),
                        $('#ApellidoMaterno').val(),   
                        $('#Email').val(),
                        $('#Celular').val(),
                        $('#DireccionAcademica').val(), 
                        $('#AñoIngreso').val()             //recupero el valor del input
                        ])
                }, 2000)

            })          
        }
    }).then(function (result){

     console.log(result);

     $.ajax({
        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
        url: '{{route('storeStudent')}}',
        type: 'POST', //
        dataType: 'JSON',
        data: { 
            rutAlumno:result[0],
            idEstado:result[1],
            Nombre:result[2],
            ApellidoPaterno:result[3],
            ApellidoMaterno:result[4],
            Email:result[5],
            Celular:result[6],
            DireccionAcademica:result[7],
            AñoIngreso:result[8],
            Active:1
        },
        beforeSend: function () {

            swal({
                title: 'Espere...',
                text: 'Registrando Carrera',
                onOpen: () => {
                    swal.showLoading()
                }
            }).then((result) => {
                if (result.dismiss === 'timer') {
                }
            })
        },
    }).done(function( data ) {

      console.log(data);

      swal(
        '¡Enviado!',
        'La carrera ha sido agregado con exito',
        'success'
        ).then(function () {
            location.reload();
        })
    }).fail(function(data) {
        swal(
            '¡Error!',
            'La carrera no pudo ser agregado, favor intente más tarde',
            'error'
            )
    });


}).catch(swal.noop)
});
</script>
@endsection