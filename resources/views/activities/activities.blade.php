@extends('layouts.app')
@section('contenido')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header">
            <strong>Actividad</strong> Formulario
        </div>
            <div class="card-body card-block">
                <form action="{{Route('addActivities')}}"  method="post" enctype="multipart/form-data" class="form-horizontal">
                     {{ csrf_field() }}

                    <!-- -->
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="select" class=" form-control-label">Agrupación</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <select name="idDirectiva" id="select" class="form-control">
                                    @foreach ($directiva as $directivas)
                                    <option value="{{$directivas->idDirectiva}}">{{$directivas->NombreRepresentacion}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 
                             <!-- -->
                        
                           <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="number-input" class=" form-control-label">Nombre</label>
                        </div>
                            <div class="col-12 col-md-9">
                                <input id="Nombre" type="text-input" id="Nombre" name="Nombre" placeholder="Nombre Actividad" class="form-control" required>
                                    <small class="form-text text-muted">Ingrese el nombre de la actividad</small>
                            </div>
                    </div>
                    <!-- -->
                    
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="date" class=" form-control-label"  >Fecha Actividad</label>
                        </div>
                            <div class="col-12 col-md-9">
                                <input id="Fecha" type="date" name="Fecha"  min="2019-08-14" value="2019-08-14"  placeholder="fecha" class="form-control" required>
                                    <small class="form-text text-muted">Seleccione fecha de la actividad </small>
                            </div>
                    </div>
                    <!-- -->

            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="textarea-input" class=" form-control-label">Objetivo de la actividad</label>
                </div>
                    <div class="col-12 col-md-9">
                        <textarea  name="ObjetivoActividad" id="ObjetivoActividad" rows="9" placeholder="Describa brevemente el objetivo de la actividad" class="form-control" required></textarea>
                    </div>
            </div>
            <!-- -->

            <div class="card-footer">
                        <center> 
                            <button type= "reset" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Guardar
                            </button>     
                            <button type="submit" class="btn btn-success btn-sm">
                                <i class="far fa-envelope"></i> Enviar
                            </button>
                        </center>
                    </div>
        <!--    // el metal es el mas lindo-->

@endsection


@section('scriptFooter')
<script type="text/javascript">

function swal(){
 swal(
        '¡Enviado!',
        'El Alumno ha sido agregado con exito',
        'success'
        ).then(function () {
            location.reload();
        })
}

</script>
@endsection